#!bin/python

import os
import requests
import json
import urllib.parse
import argparse
import time

# Global variables
file_name_index = 0

# Set up ArgParse to take user-supplied arguments
parser = argparse.ArgumentParser(
    description='Configuration options for the Github Dork Scanner')
parser.add_argument(
    '-o',
    '--organization', 
    nargs=1, 
    help=(
        'The organization to scan. If no organization is provided '
        'then a general search will be performed.'), 
    action='store', 
    dest='org', 
    required=False)
parser.add_argument(
    '-q',
    '--query',
    nargs=1,
    help=(
        'Either the invdividual query string or the path to a file '
        'containing multiple query strings.'),
    action='store',
    dest='query',
    required=True)
parser.add_argument(
    '-r',
    '--remove-previous',
    help='Removes any previous scan results from the results/ directory',
    action='store_true',
    dest='remove',
    required=False)
args = parser.parse_args()


""" remove_previous_results
Function to remove previous results files from the results/ directory

"""
def remove_previous_results():
    print('Removing previous result files...')

    directory = 'results/'
    
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        os.unlink(file_path)


""" read_dork_file
Function to read the user-supplied file path argument

Parameters
----------
dork_file : <list>
    - A list containing the search queries to try
"""
def read_dork_file(dork_file):
    file = open(dork_file)
    dorks = file.readlines()
    file.close()

    return dorks


""" read_token
Function to read the .token file which contains the personal access token (PAT)
to authenticate with Github

Parameters
----------
dork_file : <list>
    - A list containing the search queries to try
"""
def read_token():
    with open('.token', encoding="utf-8") as f:
        read_data = f.read()
    
    return read_data.strip()


""" process_query
Function to capture the process for creating the query string, capturing the
data object, and writing to a local file.

Parameters
----------
org : <str>
    - The organization or individual username for the account to query
query : <str>
    - The url-encoded query string
token : <str>
    - The PAT which was read into memory previously
"""
def process_query(org, query, token):
    print(f"Currently searching for '{query}'...")
    query_string = create_query_string({"org":args.org,"query":query.strip()})
    data = query_search(token, query_string)

    if data is not None:
        write_file(data)
    else:
        print("No results found!")
    

""" create_query_string
Function to create the proper query string using the user-supplied values.
The final query string is url-encoded.

Parameters
----------
query_object : <dict>
    - A dictionary containing the org username and the search query
"""
def create_query_string(query_object):
    query_string = f'"{query_object["query"]}"'

    if query_object["org"] is not None:
        query_string = f'org:{query_object["org"][0]}  {query_string}'

    query_string = f'{urllib.parse.quote(query_string)}&type=code'

    return query_string


""" query_search
Function which makes the HTTP request to the Github API

Parameters
----------
token : <str>
    - The PAT which was read into memory previously
query_string : <str>
    - The current query string, url-encoded
"""
def query_search(token, query_string):
    results = None
    
    url = f'https://api.github.com/search/code?q={query_string}'
    headers = {
        'Accept':'application/vnd.github+json',
        'Authorization': f'Bearer {token}',
        'X-GitHub-Api-Version':'2022-11-28'
    }

    r = requests.get(url, headers=headers)
    json_object = json.loads(r.text)
    json_object.update({"query-string":query_string})

    return json_object if json_object['total_count'] > 0 else None


""" write_file
Function which writes the results in JSON format to a text file.

Parameters
----------
data : <dict>
    - the JSON object to be written to the text file
"""
def write_file(data):   
    global file_name_index

    print(f"Creating 'results/{file_name_index}-query.json'...")
    
    with open(
        f'results/{file_name_index}-query.json', "w+", encoding="utf-8") as f:
        f.write(json.dumps(data))

    file_name_index = file_name_index + 1


""" main
"""
def main():
    token = read_token()

    if hasattr(args, 'remove'):
        remove_previous_results()

    try:
        query = read_dork_file(args.query[0])
        for q in query:
            process_query(args.org, q.strip(), token)
            #Github API limits code searches to 10 per minute so we sleep
            #every 10 seconds
            time.sleep(10)
    except IOError:
        query = args.query[0]
        process_query(args.org, query.strip(), token)
    

if __name__ == "__main__":
    main()