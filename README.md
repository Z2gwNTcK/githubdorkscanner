# Github Dork Scanner
This is a script which will take either a singular search query or a list of search queries read in from a file and perform code-level searches on Github. The searches can be either specific to an organization or can be organization agnostic.

## Setup
You will need to create a Personal Access Token (PAT) in Github in order to perform these searches. You can [read more about creating the PAT here](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens).

It is highly reccommended that you create a virtual environment within your directory to encapsulate your dependncies. You can [read more about creating virtual environments in Python here](https://docs.python.org/3/library/venv.html). the quickest way to do this is the following. From _outside of the repository directory_, run the following:
```python3 -m venv githubdorkscanner```

Once created, you will need to activate your virtual environment. You can do this by running the following:
```source githubdorkscanner/bin/activate```

There is one dependency which needs to be installed as it does not come natively and that is [requests](https://pypi.org/project/requests/). You can do this by running the following command:
```pip install requests```

Personally, I like to add execution permissions to my Python scripts to save me from having to prefix all of my commands with the interpreter (in this case Python). You can do this by running the following _from within the repository directory_.
```chmod +x githubdorkscanner/scanner.py```

## Running the Script
The script is run by executing `scanner.py`. Note that there are a number of commandline arguments that can be passed. You can learn about these by running `./scanner.py --help`:

```
usage: scanner.py [-h] [-o ORG] -q QUERY [-r]

Configuration options for the Github Dork Scanner

options:
  -h, --help            show this help message and exit
  -o ORG, --organization ORG
                        The organization to scan. If no organization is provided then a general search will be performed.
  -q QUERY, --query QUERY
                        Either the invdividual query string or the path to a file containing multiple query strings.
  -r, --remove-previous
                        Removes any previous scan results from the results/ directory
```

Here are two examples:

This first example searches your org for the existance of the string `PASSWORD=` in your repositories.
```./scanner.py -o YOUR_ORG_ID -q "PASSWORD="```

This example searches your org for the existance of any of the Github dork strings in any of your repositories. Note that the API limits these requests to 10 per minute.
```./scanner.py -o YOUR_ORG_ID -q dorks/github-dorks.txt```